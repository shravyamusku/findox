﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Allvue.InvescoBL.Findox.Plugins
{
    public class RestrictedList
    {
        public string IssuerId { get; set; }
        public string IsRestricted { get; set; }
        public string EverestIssuerId { get; set; }
        public int RestrictionIds { get; set; }
        public string FlagIds { get; set; }
        public string FlagNames { get; set; }
        public string EverestIssuerName { get; set; }
        public string IssuerName { get; set; }
    }
}
