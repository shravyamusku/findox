﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Allvue.InvescoBL.Findox.Plugins
{
	internal class RequestUtil
	{
		private static HttpClient client;

		public static bool MakeRequest(string findoxtoken, string findoxsecret, string publicAPI,string privateAPI,string signInApi,string uri)
		{
			var handler = new HttpClientHandler() { UseCookies = false };
			client = new HttpClient(handler) { BaseAddress = new Uri(uri + '/') };
			client.DefaultRequestHeaders.Add("Origin", uri);
			client.DefaultRequestHeaders.Add("Accept", "application/json");

			// Login
			var r = ApiJson(signInApi,
				new
				{
					token = findoxtoken,
					secret = findoxsecret
				});
			var token = (string)r["data"]["token"]["token"];
			var csrf = (string)r["data"]["token"]["data"]["forgerytoken"];
			string userId = r["data"]["token"]["data"]["user_id"].ToString();
			client.DefaultRequestHeaders.Add("X-Csrf-Token", csrf);
			client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

			client.DefaultRequestHeaders.Add("user-id", userId);
			client.DefaultRequestHeaders.Add("is-snake", "false");

            try
            {

				var holdingsPivateRes = ApiJson(privateAPI, new { });
				var holdingsRes = ApiJson(publicAPI, new { });

				return jsonStringToCSV(holdingsPivateRes.ToString(), holdingsRes.ToString());
			}
			catch(Exception)
            {
				return false;
            }


		}

		public static bool jsonStringToCSV(string jsonPrivateContent, string jsonContent)
		{
			DataSet dataSet = XMLString(jsonPrivateContent);
			DataSet dataSetPublic = XMLString(jsonContent);


			if (!(dataSet.Tables.Count <= 1 && dataSetPublic.Tables.Count <= 1))
			{
				DataTable dataTableIssuer;
				DataTable dataTableRestrictions;
				DataTable dataTableFlagIds;
				DataTable dataTableFlagNames;


				DataTable dataTablePublic;

				try
				{
					dataTableIssuer = dataSet.Tables[0];
					dataTableRestrictions = dataSet.Tables[9];  // Restrictions
					dataTableFlagIds = dataSet.Tables[10];  // FlagIds
					dataTableFlagNames = dataSet.Tables[11];  // FlagNames


					dataTablePublic = dataSetPublic.Tables[0];

					JObject o1 = JObject.FromObject(
						new
						{
							item = (
				  from p in dataTableIssuer.AsEnumerable()
				  join t in dataTableRestrictions.AsEnumerable()
				   on p.Field<string>("IssuerId") equals t.Field<string>("IssuerId")
				  join f in dataTableFlagIds.AsEnumerable()
				   on t.Field<int>("Restrictions_Id") equals f.Field<int>("Restrictions_Id")
				  join n in dataTableFlagNames.AsEnumerable()
				   on f.Field<int>("Restrictions_Id") equals n.Field<int>("Restrictions_Id")
				  select new RestrictedList()
				  {
					  IssuerId = p.Field<string>("IssuerId"),
					  IsRestricted = p.Field<string>("IsRestricted"),
					  EverestIssuerId = p.Field<string>("ClientCustomId"),
					  RestrictionIds = t.Field<int>("Restrictions_Id"),
					  FlagIds = f.Field<string>("FlagIds_Text"),
					  FlagNames = n.Field<string>("FlagNames_Text"),
					  EverestIssuerName = p.Field<string>("ClientIssuerName"),
					  IssuerName = p.Field<string>("IssuerName")

				  })
						});

					JObject o2 = JObject.FromObject(
						new
						{
							item = (
				  from pu in dataTablePublic.AsEnumerable()
				  select new RestrictedList()
				  {
					  IssuerId = pu.Field<string>("IssuerId"),
					  IsRestricted = pu.Field<string>("IsRestricted"),
					  EverestIssuerId = pu.Field<string>("ClientCustomId"),
					  RestrictionIds = 0,
					  FlagIds = "0",
					  FlagNames = "",
					  EverestIssuerName = "",
					  IssuerName = pu.Field<string>("IssuerName")

				  })
						});

					o1.Merge(o2, new JsonMergeSettings
					{
						// union array values together to avoid duplicates
						MergeArrayHandling = MergeArrayHandling.Union
					});

					DataSet dataSetfinal = XMLString("{data:" + o1 + "}");

					var finaldatatable = dataSetfinal.Tables[1];
					//var res = JoinResult.Select(x => x.)

					// Datatable to CSV

					var lines = CSVHeader(finaldatatable);

					File.WriteAllLines(@Properties.Settings.Default.UATFile, lines);
					return true;

				}
				catch (Exception e)
				{
					List<string> error = new List<string>();
					error.Add(e.GetType().Name);
					error.Add(e.Message);
					File.WriteAllLines(@Properties.Settings.Default.UATError, error);
					return false;
				}
			}
			else
			{
				var dataTableError = dataSet.Tables[0];

				var lines = CSVHeader(dataTableError);
				File.WriteAllLines(@Properties.Settings.Default.UATError, lines);

			} 

            return false;
		}

		private static DataSet XMLString(string jsonContent)
		{
			XmlNode xml = JsonConvert.DeserializeXmlNode("{data:" + jsonContent + "}");
			XmlDocument xmldoc = new XmlDocument();
			xmldoc.LoadXml(xml.InnerXml);
			XmlReader xmlReader = new XmlNodeReader(xml);
			DataSet dataSet = new DataSet();
			dataSet.ReadXml(xmlReader);
			return dataSet;
		}
		private static JObject ApiJson(string url, object request)
		{
			var res = Api(url, request);
			var resJson = res.Content.ReadAsStringAsync().Result;
			if (res.StatusCode == System.Net.HttpStatusCode.Unauthorized)
			{
				Console.WriteLine("unauthorized");
				return null;
			}
			return JObject.Parse(resJson);
		}

		private static HttpResponseMessage Api(string url, object request)
		{
			var reqJson = JsonConvert.SerializeObject(request);

			var res = client.PostAsync("apiv3/" + url, new StringContent(reqJson, Encoding.UTF8, "application/json")).Result;
			return res;
		}

		private static List<string> CSVHeader(DataTable dataTable)
		{
			var lines = new List<string>();

			string[] columnNames = dataTable.Columns.Cast<DataColumn>().
											  Select(column => column.ColumnName).
											  ToArray();
			var header = string.Join(",", columnNames);
			lines.Add(header);
			var valueLines = dataTable.AsEnumerable()
							.Select(row => string.Join(",", row.ItemArray.Select(s => "\"" + s + "\"")));
			lines.AddRange(valueLines);
			return lines;
		}
		
	}
}