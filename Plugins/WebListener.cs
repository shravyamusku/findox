﻿using System;
using System.Collections.Generic;
using System.IO;
using BMS.Common.Plugin.Contract.Content;
using BMS.LTD.B2B.Plugins.Import.Listener;

namespace Allvue.InvescoBL.Findox.Plugins
{
	public class WebListener : PollingListenerPlugin
	{

		public string token { get; set; }
		public string secret { get; set; }
		public string publicAPI { get; set; }
		public string privateAPI { get; set; }
		public string signInApi { get; set; }
		public string uri { get; set; }

		#region Overrides of PollingListenerPlugin

		public override bool Poll(ref ContentListenRequest request)
        {
			//TODO: do request with token
			try
			{
				Log(Level.Debug, $"Download data: {request.ListenUri}");

				return RequestUtil.MakeRequest(token,secret,publicAPI,privateAPI, signInApi,uri);

			}
			catch (Exception e)
			{
				Log(Level.Error, $"Error in request: {request.ListenUri}", e);
				List<string> error = new List<string>();
				error.Add(e.GetType().Name);
				error.Add(e.Message);
				File.WriteAllLines(@Properties.Settings.Default.DEVError, error);
				return false;
			}

		}
		#endregion
	}
}